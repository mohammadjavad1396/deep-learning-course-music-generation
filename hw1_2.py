from music21 import converter, instrument, note, chord
import glob
import os
import numpy as np
from keras.utils import np_utils

def get_notes():
    notes = []
    for file in glob.glob('data' + os.sep + '*.mid'):
        midi = converter.parse(file)
        parts = instrument.partitionByInstrument(midi)
        # print(parts.parts[0].recurse())
        if parts:  # file has instrument parts
            notes_to_parse = parts.parts[0].recurse()
        else:  # file has notes in a flat structure
            notes_to_parse = midi.flat.notes

        for element in notes_to_parse:
            if isinstance(element, note.Note):
                notes.append(str(element.pitch))
            elif isinstance(element, chord.Chord):
                notes.append('.'.join(str(n) for n in element.normalOrder))

    return notes

def prepare_seq(notes, n_vocab):
    sequence_length = 50
    # get all pitch names
    pitchnames = sorted(set(item for item in notes))
    # create a dictionary to map pitches to integers
    note_to_int = dict((note, number) for number, note in enumerate(pitchnames))
    network_input = []
    network_output = []
    # create input sequences and the corresponding outputs
    for i in range(0, len(notes) - sequence_length, 1):
        sequence_in = notes[i:i + sequence_length]
        sequence_out = notes[i + sequence_length]
        network_input.append([note_to_int[char] for char in sequence_in])
        network_output.append(note_to_int[sequence_out])
    n_patterns = len(network_input)
    # reshape the input into a format compatible with LSTM layers
    normalize_input = np.reshape(network_input, (n_patterns, sequence_length, 1))
    # normalize input
    normalize_input = normalize_input / float(n_vocab)
    network_output = np_utils.to_categorical(network_output)
    return network_input, normalize_input, network_output

notes = get_notes()
# network_input, network_output = prepare_seq(notes, len(set(notes)))