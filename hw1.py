from urllib.request import urlopen, urlretrieve
from bs4 import BeautifulSoup
import os


save_dir = '.' + os.sep + 'Data' + os.sep
if save_dir not in os.listdir():
    os.mkdir(save_dir)


url0 = 'https://www.mutopiaproject.org/cgibin/make-table.cgi?startat='
url1 = '&searchingfor=&Composer=&Instrument=' \
       'Guitar&Style=&collection=&id=&solo=&recent=&timelength=&timeunit=&lilyversion=&preview='

song_number = 0
link_count = 10
lim = 10
cnt = 0

while link_count > 0 and cnt < 10:
    cnt += 1
    url = url0 + str(song_number) + url1
    html = urlopen(url)
    soup = BeautifulSoup(html.read(), features="html.parser")
    links = soup.find_all('a')
    link_count = 0
    for link in links:
        href = link['href']
        if href.find('.mid') >= 0:
            link_count += 1
            urlretrieve(href, save_dir + str(cnt) + '_' + str(link_count) + '.mid')

    song_number += 10

