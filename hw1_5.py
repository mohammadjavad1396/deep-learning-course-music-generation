import os
import numpy as np

from hw1_2 import get_notes, prepare_seq
from hw1_3 import creat_model_LSTM


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def generate():
    notes = get_notes()
    n_vocab = len(set(notes))
    network_input, normalize_input, _ = prepare_seq(notes, n_vocab)
    model = creat_model_LSTM(normalize_input, n_vocab)
    file = os.listdir('model')[-1]

    x = 'weights-improvement-179-0.1109-bigger.hdf5'
    # test = []
    # for item in os.listdir('model'):
    #     if item.endswith('.hdf5'):
    #         if RepresentsInt(item[22]) == True:
    #             test.append(item)
    # print(test[-1])
    model.load_weights('model' + os.sep + x)
    start = np.random.randint(0, len(network_input) - 1)
    pitchnames = sorted(set(item for item in notes))
    int_to_note = dict((number, note) for number, note in enumerate(pitchnames))
    pattern = network_input[start]
    prediction_output = []  # generate 500 notes
    for note_index in range(150):
        prediction_input = np.reshape(pattern, (1, len(pattern), 1))
        prediction_input = prediction_input / float(n_vocab)
        prediction = model.predict(prediction_input, verbose=0)
        index = np.argmax(prediction)
        result = int_to_note[index]
        prediction_output.append(result)
        pattern.append(index)
        pattern = pattern[1:len(pattern)]

    return prediction_output

generate()