from keras.layers import SimpleRNN
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers.cudnn_recurrent import CuDNNLSTM
from keras.layers import LSTM
from hw1_2 import prepare_seq, get_notes
import os
import keras


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def creat_model_LSTM(network_input, n_vocab):
    keras.backend.clear_session()
    model = Sequential()
    model.add(LSTM(
        256,
        input_shape=(network_input.shape[1], network_input.shape[2]),
        return_sequences=True
    ))
    model.add(Dropout(0.3))
    model.add(LSTM(512, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(256))
    model.add(Dense(256))
    model.add(Dropout(0.3))
    model.add(Dense(n_vocab))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    return model

def creat_model_RNN(network_input, n_vocab):
    keras.backend.clear_session()
    model = Sequential()
    model.add(SimpleRNN(
        256,
        input_shape=(network_input.shape[1], network_input.shape[2]),
        return_sequences=True
    ))
    model.add(SimpleRNN(512, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(SimpleRNN(256))
    model.add(Dense(256))
    model.add(Dropout(0.3))
    model.add(Dense(n_vocab))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    return model

# notes = get_notes()
# _, network_input, network_output = prepare_seq(notes, len(set(notes)))
# creat_model_RNN(network_input, len(set(notes)))