import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from keras.callbacks import ModelCheckpoint
import tensorflow as tf

from hw1_2 import get_notes, prepare_seq
from hw1_3 import creat_model_LSTM, creat_model_RNN


def train_net():
    filepath = "model" + os.sep + "weights-improvement-{epoch:02d}-{loss:.4f}-bigger.hdf5"
    if 'model' not in os.listdir():
        os.mkdir('model')


    checkpoint = ModelCheckpoint(
        filepath, monitor='loss',
        verbose=0,
        save_best_only=True,
        mode='min'
    )

    notes = get_notes()
    n_vocab = len(set(notes))
    _, network_input, network_output = prepare_seq(notes, n_vocab)
    # print(network_input.shape, network_output.shape)
    # network_input, network_output = network_input[:100], network_output[:100]
    # print(network_input.shape)
    model = creat_model_LSTM(network_input, n_vocab)
    callbacks_list = [checkpoint]
    model.fit(network_input, network_output, epochs=200, batch_size=32, callbacks=callbacks_list)
