from music21 import instrument, note, stream, chord
from hw1_5 import generate

def create_midi():
    print('Creating midi...')
    """ Convert prediction output to notes. Create midi file!!!! """
    prediction_output = generate()
    offset = 0
    output_notes = []
    # Possible extension: multiple/different instruments!
    stored_instrument = instrument.Saxophone()

    # Create Note and Chord objects
    for pattern in prediction_output:
        # Pattern is a Chord
        if ('.' in pattern) or pattern.isdigit():
            notes_in_chord = pattern.split('.')
            notes = []
            for current_note in notes_in_chord:
                new_note = note.Note(int(current_note))
                new_note.storedInstrument = stored_instrument
                notes.append(new_note)

            new_chord = chord.Chord(notes)
            new_chord.offset = offset
            output_notes.append(new_chord)
        else: # Pattern is a note
            new_note = note.Note(pattern)
            new_note.offset = offset
            new_note.storedInstrument = stored_instrument
            output_notes.append(new_note)

        # Increase offset for note
        # Possible extension: ~ RHYTHM ~
        offset += 0.5

    midi_stream = stream.Stream(output_notes)
    midi_stream.write('midi', fp='output_song.mid')

create_midi()