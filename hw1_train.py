from hw1_4 import train_net
from tensorflow.python.client import device_lib
import tensorflow as tf

# print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
# print(device_lib.list_local_devices())
#
if __name__ == '__main__':
    train_net()
